# Moziware LaserLamp Demo

[点击切换为中文](./README.md)

## Description
This project shows how to use the Laser Lamp of CIMO equipment

## Operating requirements
- Support Version: Infinity OS > 10.7.1
- Hardware requirement: CIMO device that supports laser lamp, Usually the fuselage has the following label
  <img src='resource/laserlamplabel.png' width="70%" height="70%"/>

## Look up API
- For details about the API documents used in the Demo, see: [Cimo-Guide-Docs ](https://gitee.com/moziware/cimo-guide-docs)