# Moziware LaserLamp Demo

[Click here to change to English](./README-en.md)

## 简述
本项目是CIMO设备的Laser Lamp使用Demo

## 使用要求
- 支持的版本：Infinity OS > 10.7.1
- 硬件要求：可以使用激光灯的CIMO设备；通常机身有以下标签
  <img src='resource/laserlamplabel.png' width="70%" height="70%"/>

## 查阅API
- Demo中使用的API文档请查询：[Cimo-Guide-Docs ](https://gitee.com/moziware/cimo-guide-docs)