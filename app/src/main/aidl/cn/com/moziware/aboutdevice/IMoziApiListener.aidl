// IMoziApiListener.aidl
package cn.com.moziware.aboutdevice;

interface IMoziApiListener {
    void callback(int code, String msg, String data);
}