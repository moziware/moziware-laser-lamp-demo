// IMoziApiService.aidl
package cn.com.moziware.aboutdevice;

import cn.com.moziware.aboutdevice.IMoziApiListener;

interface IMoziApiService {
    void handler(in String method, in String params, IMoziApiListener listener);
}